import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:camera/camera.dart';

List<CameraDescription> cameras;

Future<void> main() async{
  cameras = await availableCameras();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Selfie Arm big Alpha'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Go detect some bois'),
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => _DetectBoi(),
              )
            );
          },
        ),
      ),
    );
  }
}

class _DetectBoi extends StatelessWidget{

  Widget build(BuildContext context){
    return Scaffold(
      body:DetectionPreview()
    );
  }
}



class DetectionPreview extends StatefulWidget{
  DetectionPreview({
    Key key,
  }) : super (key: key);

  _DetectionPreviewState createState() => _DetectionPreviewState();
}

class _DetectionPreviewState extends State<DetectionPreview>{

  List<Face> faces;
  CameraController _controller = CameraController(cameras[1], ResolutionPreset.medium);
  final _detector = FirebaseVision.instance.faceDetector().processImage;
  int _rotationIndex = 3;
  bool _processing = false;
  bool _isCameraReady = false;


  void initState(){
    super.initState();
    _initialise();
  }

  Future<void> _initialise() async{
    await _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
    _isCameraReady = true;
    _controller.startImageStream(_processImage);
  }

  void _processImage(CameraImage image) async{

    if(!_processing){
      _processing = true;
      try{
        final faces = await _detect(image);
        int newRotation = _rotationIndex;

        if(faces.isNotEmpty) {
          final rotation = faces.first.headEulerAngleZ;
          if(rotation >= 50 )
            newRotation = newRotation < 3 ? newRotation + 1 : 0;
          else if(rotation <= -50)
            newRotation = newRotation > 0 ? newRotation - 1 : 3;
        }

        setState(() {
          this.faces = faces;
          _rotationIndex = newRotation;
        });

      }catch(ex, stack){
        debugPrint('$ex, $stack');
      }
      _processing = false;
    }

  }

  Future<List<Face>> _detect(CameraImage image) async{
    return _detector(
        FirebaseVisionImage.fromBytes(
          _concatenatePlanes(image.planes),
          _buildMetadata(image),
        )
    );
  }

  Uint8List _concatenatePlanes(List<Plane> planes){
    final WriteBuffer allBytes = WriteBuffer();
    planes.forEach((plane) => allBytes.putUint8List(plane.bytes));
    return allBytes.done().buffer.asUint8List();
  }

  FirebaseVisionImageMetadata _buildMetadata(CameraImage image){
    return FirebaseVisionImageMetadata(
      rawFormat: image.format.raw,
      rotation: ImageRotation.values[_rotationIndex],
      size: Size(image.width.toDouble(), image.height.toDouble()),
      planeData: image.planes.map(
              (plane) => FirebaseVisionImagePlaneMetadata(
            bytesPerRow: plane.bytesPerRow,
            height: plane.height,
            width: plane.width,
          )
      ).toList(),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  Widget build(BuildContext context){
    if(!_isCameraReady){
      return Center(
        child: Icon(Icons.donut_large),
      );
    }

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final previewHeight = screenWidth * _controller.value.previewSize.aspectRatio;


    Widget cameraPreview = Transform.scale(
      alignment: Alignment.topCenter,
      scale: 1,
      child: SizedBox(
        width: screenWidth,
        height: previewHeight,
        child: CustomPaint(
          child: CameraPreview(_controller),
          foregroundPainter: _getPainter(),
        ),
      )
    );

    return Center(
      child: cameraPreview,
    );
  }



  _Overlay _getPainter() {
    if (faces != null) {
      return _Overlay(
          rectangles: faces.map(
                  (face) => face.boundingBox
          ).toList(),
          ratio: _controller.value.aspectRatio
      );
    }
    else
      return null;
  }
}


class _Overlay extends CustomPainter{

  _Overlay({this.rectangles, this.ratio});

  final List<Rect> rectangles;
  final ratio;

  @override
  void paint(Canvas canvas, Size size) {
    if(rectangles != null) {
      rectangles.forEach(
              (rectangle) {
            final reversed = Rect.fromLTRB(
                size.width - rectangle.left * ratio,
                rectangle.top * ratio,
                size.width - rectangle.right * ratio,
                rectangle.bottom * ratio
            );

            canvas.drawRect(
                reversed,
                Paint()
                  ..color = Colors.red
                  ..style = PaintingStyle.stroke
            );
          }
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }




}